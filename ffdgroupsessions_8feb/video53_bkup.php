<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="//player.wowza.com/player/latest/wowzaplayer.min.js"></script>

</head>

<body>
<div id="videoPlayer"></div>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
myPlayer = WowzaPlayer.create('videoPlayer',
    {
    "license":"PLAY2-f9DvN-9hUXF-64dZB-j3Vaw-ERHJp",
    "sourceURL":"https://wowzaprod256-i.akamaihd.net/hls/live/1006585/5ae21302/playlist.m3u8",
    "autoPlay":false,
    "volume":"75",
    "mute":false,
    "loop":false,
    "audioOnly":false,
    "stringErrorStreamUnavailable" : "Please try again later.",
    "posterFrameURL" : "img/poster09.jpg",
    }
);
//myPlayer.play();
</script>
</body>
</html>