<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#videoPlayer{
    width:100%;
    height:100%;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<div id="videoPlayer"></div>
<script src="js/jquery.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>

<script>
  var playerElement = document.getElementById("videoPlayer");

  var player = new Clappr.Player({
    source: 'https://wowzaprod272-i.akamaihd.net/hls/live/1007426/fe1a7efe/playlist.m3u8',
    height: '100%',
    width: '100%',
    autoPlay: false,
    poster:'img/poster.jpg',
    plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              4: '720p', // 500kbps
              3: '480p', // 240kbps
              2: '360p', // 240kbps
              1: '288p', // 240kbps
              0: '180p', // 120kbps
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },  
        
  });
  
  player.attachTo(playerElement);
  //player.play();
       
</script>
</body>
</html>