<?php
	require_once "config.php";
	
	if(!isset($_SESSION["user_phone"]))
	{
		header("location: index.php");
		exit;
	}
    if(($_SESSION["batch"] != 'batch58'))
	{
		header("location: index.php");
		exit;
	}
	
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
            $phone=$_SESSION["user_phone"];
            $code=$_SESSION["user_code"];
            $batch=$_SESSION["batch"];
            
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where cntry_code='$code' and mobile_num='$phone' and batch ='$batch'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["user_phone"]);
            unset($_SESSION["user_code"]);
            unset($_SESSION["batch"]);
            
            header("location: index.php");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Freedom From Diabetes Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body class="<?php echo $_SESSION['batch']; ?>">
<div class="container-fluid top-nav">
    <div class="row">
        <div class="col-12 col-md-3">
            <a class="navbar-brand" href="#"><img src="img/logo.png" class="logo"></a>
        </div>
        <div class="col-12 col-md-6 text-center">
            <h5>Intensive Batch 58</h5>
            <h6>1st Group Session Live</h6>
            <h6>9th May 2020, 7am to 10am (IST)</h6>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mt-2 mb-2 info">
        <div class="col-12 text-right">
            Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout">Logout</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-12 col-md-4 text-center">
            <img src="img/02.jpg" class="img-fluid dr-photo"  alt=""/> 
        </div>

        <div class="col-12 col-md-8 text-center">
            <!--<div class="tabs">
                <div class="row m-0">
                    <div class="col-4 col-md-3 p-0">
                        <h4 class="vid-link p act" onClick="changeVideo('video58.php','p')"><a href="#">Primary</a></h4>
                    </div>
                    <div class="col-4 col-md-3 p-0">
                        <h4 class="vid-link s" onClick="changeVideo('video58_bkup.php','s')"><a href="#">Secondary</a></h4>
                    </div>
                </div>
            </div>-->
            <div class="embed-responsive embed-responsive-16by9 video-panel">
              <iframe class="embed-responsive-item" id="webcast" src="video58.php" allowfullscreen></iframe>
            </div>
            <div id="question">
              <div id="question-form" class="panel panel-default">
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="1"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="user_phone" name="user_phone" value="<?php echo $_SESSION['user_phone']; ?>">
                          <input type="hidden" id="user_code" name="user_code" value="<?php echo $_SESSION['user_code']; ?>">
                          <input type="hidden" id="user_batch" name="user_batch" value="<?php echo $_SESSION['batch']; ?>">
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
            
        </div>
    </div>
    <div class="row mt-3 mb-3 top-nav">
        <div class="col-12 text-center">
            <div class="icons">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){
	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ update(); }, 30000);
function changeVideo(video, l)
{
    var vid = '.'+l;
    $('.vid-link').removeClass('act');
    $(vid).addClass('act');
    
    $('#webcast').attr("src",video);
    
    return false;
    
}
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>