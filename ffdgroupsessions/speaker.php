<?php
	require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Questions for Doctor</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">

</head>
<body class="admin">
<div class="container-fluid main">
     
  <div class="row mt-5 mb-5 batches">
        <div class="col-6 col-md-6 text-right"> <a href="speaker_batch56.php"><img src="img/batch56.jpg" class="img-fluid" alt=""/></a> 
      </div>
        <div class="col-6 col-md-6 text-left"> <a href="speaker_batch55.php"><img src="img/batch55.jpg" class="img-fluid" alt=""/></a> 
        </div>
        <div class="col-6 col-md-6 text-right"> <a href="speaker_batch53.php"><img src="img/batch53.jpg" class="img-fluid" alt=""/></a> 
        </div>
        <div class="col-6 col-md-6 text-left"> <a href="speaker_batch54.php"><img src="img/batch54.jpg" class="img-fluid" alt=""/></a> 
        </div>
    </div>
</div>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
</body>
</html>